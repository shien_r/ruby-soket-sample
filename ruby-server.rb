require 'socket'
require 'thread'

# 1.サーバ用のソケットを開いて
# 2.アクセス毎にスレッド作って
# 3.メッセージを受け取る

#ファイルディスクリプタをかえす
#ポート番号 5590 でまつ
gs = TCPServer.open(5590)

#接続に使用する情報のリスト
addr = gs.addr

#リストの先頭から情報を取り出す
addr.shift

p("server is on" + addr.join(":"))

while true 
    #thread のソケットをアクセスされた事に作成
    Thread.start(gs.accept) do |s| 
        print(s, " is accepted\n")
        puts(s.gets) #送信
        print(s, " is gone\n")
        #ソケット閉じる
        s.close
    end
end
